﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ecommerce_management.Models;
using Newtonsoft.Json;

namespace ecommerce_management
{
    public partial class GetJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string s = JsonConvert.SerializeObject(getBooks());
            //
            Response.ContentType = "application/json";
            Response.Write(s);
        }

        private dynamic getBooks()
        {
            ecommerce_management.Models.EcommerceDBEntities DB = new Models.EcommerceDBEntities();
            return DB.Books.Select( b => new { id = b.id,price = b.prix, categ = b.nom});
        }
    }
}