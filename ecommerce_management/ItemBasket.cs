﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce_management
{
    public class ItemBasket
    {
        public int BookID { get; set; }
        public int Quantity { get; set; }
    }
}