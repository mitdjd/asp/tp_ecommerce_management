﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecommerce_management.Models;

namespace ecommerce_management.Controller
{
    public class CommandController
    {

        public static Commande add(Commande cmd, List<DetailCommand> detailsCommand)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            DB.Commande.Add(cmd);
            DB.SaveChanges();
            //
            foreach (var detailCommand in detailsCommand)
            {
                DetailsCommandController.add(detailCommand);
            }
            //
            return cmd;
        }

        public static Commande add(Commande cmd, List<int> books)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            DB.Commande.Add(cmd);
            DB.SaveChanges();
            //
            foreach (var book in books)
            {
                DetailsCommandController.add(book, cmd.id);
            }
            //
            return cmd;
        }

        public static Commande add(Commande cmd, List<Books> books)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            DB.Commande.Add(cmd);
            DB.SaveChanges();
            //
            foreach (var book in books)
            {
                DetailsCommandController.add(book.id, cmd.id);
            }
            //
            return cmd;
        }

        public static Commande add(DateTime creation_date, int booksCount, int price, double tva, double totalprice, List<int> books)
        {
            //var cmd = new Commande(creation_date, booksCount, price, tva, totalprice);
            ////
            //return add(cmd, books);
            return null;
        }

        public static Commande add(DateTime creation_date, int booksCount, int price, double tva, double totalprice, List<Books> books)
        {
            //var cmd = new Commande(creation_date, booksCount, price, tva, totalprice);
            ////
            //return add(cmd, books);
            return null;
        }

        public static Commande add(DateTime creation_date, int booksCount, int price, double tva, double totalprice, List<DetailCommand> details)
        {
            //var cmd = new Commande(creation_date, booksCount, price, tva, totalprice);
            ////
            //return add(cmd, details);
            return null;
        }


    }
}