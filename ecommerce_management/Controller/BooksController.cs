﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecommerce_management.Models;

namespace ecommerce_management.Controller
{
    public class BooksController
    {

        public static Books getBook(int id)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            return DB.Books.First(book => book.id == id) ;
        }

        //public IQueryable<Books> books;
        public static IQueryable<Books> getByCategI(IQueryable<Books> books, int categId)
        {
            books = books.Where(b => b.category_id == categId);
            return books;
        }

        public static IQueryable<Books> getByCateg(int categId)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            return DB.Books.Where(b => b.category_id == categId);
        }

        public static List<Books> getByCategList(int categId)
        {
            return getByCateg(categId).ToList();
        }

        public static IQueryable<Books> getAll()
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            return DB.Books;
        }

        public static List<Books> getAllList()
        {
            return getAll().ToList();
        }

        public static IQueryable<Books> getByPriceBetweenI(IQueryable<Books> books, int price_min, int price_max)
        {
            books = books.Where(b => b.prix >= price_min && b.prix <= price_max);
            return books;
        }

        public static IQueryable<Books> getByPriceBetween(int price_min, int price_max)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            return DB.Books.Where(b => b.prix >= price_min && b.prix <= price_max);
        }

        public static List<Books> getByPriceBetweenList(int price_min, int price_max)
        {
            return getByPriceBetween(price_min, price_max).ToList();
        }

        public static IQueryable<Books> getByNameI(IQueryable<Books> books, string key)
        {
            books = books.Where(b => b.nom.Contains(key));
            return books;
        }

        public static IQueryable<Books> getByName(string key)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            return DB.Books.Where(b => b.nom.Contains(key));
        }

        public static List<Books> getByNameList(string key)
        {
            return getByName(key).ToList();
        }
    }
}