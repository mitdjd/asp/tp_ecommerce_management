﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecommerce_management.Models;

namespace ecommerce_management.Controller
{
    public class CategoriesController
    {

        public static Category GetCategory(int id)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            return DB.Category.First(category => category.id == id);
        }

    }
}