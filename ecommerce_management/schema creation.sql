use EcommerceDB;

create table Books(
	id int Identity primary key,
	nom varchar(255),
	description varchar(255),
	creation_date datetime,
	photo varchar(255),
	category_id int,
	prix int);

create table Category(
	id int Identity primary key,
	libelle varchar(255));

create table Commande(
	id int Identity primary key,
	creation_date datetime,
	booksCount int,
	price int,
	tva float,
	totalPrice float,
	);

create table DetailCommand(
	id int identity primary key,
	command_id int,
	book_id int);
