﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;

namespace ecommerce_management
{
    public partial class GetPhoto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ( ! string.IsNullOrEmpty(Request["productId"])) { 
                var id = int.Parse(Request["productId"]);
                //
                dynamic path = String.Format(@"~/Assets/images/{0}.jpg", id);
                path = Server.MapPath(path);
                //
                System.Drawing.Image img = System.Drawing.Image.FromFile(path);
                //
                Graphics graphic = Graphics.FromImage(img);
                var font = new Font("Arial", 14);
                //var font = new Font("Tahoma", 3);
                graphic.DrawString("Mother fucker its copyrighted picture", font, Brushes.Aqua, 10, 10);
                //
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] bytes = ms.GetBuffer();
                //
                Response.ContentType = "image/jpeg";
                Response.OutputStream.Write(bytes, 0, bytes.Length);
            }
        }
    }
}