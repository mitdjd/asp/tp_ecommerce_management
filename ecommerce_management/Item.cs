﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce_management
{
    public class Item
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public String Category { get; set; }
        public int prix { get; set; }

        public int QT { get; set; }

        public int PT { get; set; }
    }
}