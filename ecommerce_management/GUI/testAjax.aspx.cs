﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ecommerce_management.GUI
{
    public partial class testAjax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = DateTime.Now.ToLongTimeString();
            Label2.Text = DateTime.Now.ToLongTimeString();
            Label3.Text = DateTime.Now.ToLongTimeString();
            Label4.Text = DateTime.Now.ToLongTimeString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = DateTime.Now.ToLongTimeString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Label2.Text = DateTime.Now.ToLongTimeString();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Label3.Text = DateTime.Now.ToLongTimeString();
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Label4.Text = DateTime.Now.ToLongTimeString();
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Label3.Text = DateTime.Now.ToLongTimeString();
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Thread.Sleep(5000);
            Label3.Text = DateTime.Now.ToLongTimeString();
        }
    }
}