﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ecommerce_management.Models;

namespace ecommerce_management.GUI
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            var categories = DB.Category.ToList();
            //
            CategoriesShearchDropDownList.DataSource = categories;
            CategoriesShearchDropDownList.DataTextField = "libelle";
            CategoriesShearchDropDownList.DataValueField = "id";
            CategoriesShearchDropDownList.DataBind();
            //
            CategoriesShearchDropDownList.Items.Insert(0, new ListItem("--", "0"));
            //
            CategoriesShearchDropDownList.SelectedIndex = 0;
        }

        protected void SearchBooksSubmit_Click(object sender, EventArgs e)
        {
            var selectedCateg = CategoriesShearchDropDownList.SelectedValue;
            //
            

        }
    }
}