﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Basket.aspx.cs" Inherits="ecommerce_management.GUI.Basket" Culture="fr-FR" UICulture="ar-MA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../Assets/style/main.css" />
</head>
<body>
    <asp:ListView ID="ListView1" runat="server" >
        <ItemTemplate>
            <a href="BookDetails.aspx?book=<%#Eval("id") %>">
            <div class="book-card">
                <table>
                    <tr>
                        <td>
                            <img src="../Assets/images/<%#Eval("photo") %>" width="200"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="NameLabel" runat="server" CssClass="title" Text='<%#Eval("nom") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="price" Text='<%#Eval("prix") %>' />
                        </td>
                    </tr>
                </table>
            </div>
                </a>


        </ItemTemplate>
    </asp:ListView>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="booksPrice" runat="server" Text="Label"></asp:Label>
            <br />
            <asp:Label ID="TVA" runat="server" Text="Label"></asp:Label>
            <br />
            <asp:Label ID="totalPrice" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
