﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ecommerce_management.Controller;
using ecommerce_management.Models;

namespace ecommerce_management.GUI
{
    public partial class Basket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["basket"] != null)
            {
                List<Books> basket = (List<Books>) Session["basket"];
                if(basket.Count > 0)
                {
                    ListView1.DataSource = basket.ToList();
                    ListView1.DataBind();
                    //
                    int price = 0;
                    double tva = 0;
                    foreach (var item in basket)
                    {
                        price += item.prix.Value;
                        tva += item.prix.Value * 0.2;
                    }
                    //
                    booksPrice.Text = "Prix Net : " + price;
                    TVA.Text = "TVA 20% : " + tva;
                    totalPrice.Text = "Prix total : " + (price + tva);
                    //
                    CommandController.add(DateTime.Now, basket.Count, price, tva, (price + tva), basket);
                }
            }
        }
    }
}