﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testAjax.aspx.cs" Inherits="ecommerce_management.GUI.testAjax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            <asp:Panel ID="Panel1" runat="server" style="background:red;display:block;margin-bottom:30px">
                
                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server"  style="background:cyan;display:block;margin-bottom:30px">
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click" />
            </asp:Panel>
             <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                 <ContentTemplate>
                     <asp:Panel ID="Panel3" runat="server"  style="background:orange;display:block;margin-bottom:30px">
                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Button ID="Button3" runat="server" Text="Button" OnClick="Button3_Click" />
            </asp:Panel>
                 </ContentTemplate>
                 
        </asp:UpdatePanel>
             <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                 <ContentTemplate>
                     <asp:Panel ID="Panel4" runat="server"  style="background:green;display:block;margin-bottom:30px">
                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:Button ID="Button4" runat="server" Text="Button" OnClick="Button4_Click" />
            </asp:Panel>
                 </ContentTemplate>
                 
                 <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="Button6" EventName="Click" />
                 </Triggers>
                 
        </asp:UpdatePanel>
        </div>
       
        <asp:Button ID="Button5" runat="server" OnClick="Button5_Click" Text="Button" />
        <asp:Button ID="Button6" runat="server" OnClick="Button6_Click" Text="Button" />
        <br />
        <br />
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                Loading...
            </ProgressTemplate>
        </asp:UpdateProgress>
       
    </form>
</body>
</html>
