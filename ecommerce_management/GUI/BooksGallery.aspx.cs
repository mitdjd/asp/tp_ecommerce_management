﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ecommerce_management.Controller;

namespace ecommerce_management.GUI
{
    public partial class AllCategs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var data = Request.Form.AllKeys;
            if (data.Length > 0)
            {
                var books = BooksController.getAll();
                // by categ
                var categ = int.Parse(Request.Form[data[3]]);
                if (categ > 0)
                {
                    books = BooksController.getByCategI(books, categ);
                }
                var priceMin = 0;
                int.TryParse(Request.Form[data[4]], out priceMin);
                //
                var priceMax = 0;
                int.TryParse(Request.Form[data[5]], out priceMax);
                //
                if( priceMin<= priceMax && priceMax > 0)
                {
                    books = BooksController.getByPriceBetweenI(books, priceMin, priceMax);
                }
                //
                var searchName = Request.Form[data[6]];
                //
                if (searchName.Trim() != "")
                {
                    books = BooksController.getByNameI(books, searchName);
                }

                //
                ListView1.DataSource = books.ToList();
                ListView1.DataBind();
            } else
            {
                ListView1.DataSource = BooksController.getAllList();
                ListView1.DataBind();
                //
                //DataList1.DataSource = BooksController.getAllList();
                //DataList1.DataBind();
            }
            
            
        }
    }
}