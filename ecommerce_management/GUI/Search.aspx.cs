﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ecommerce_management.Models;

namespace ecommerce_management.GUI
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if( ! IsPostBack) { 
            EcommerceDBEntities DB = new EcommerceDBEntities();
            var categories = DB.Category.ToList();
            //
            CategoriesShearchDropDownList.DataSource = categories;
            CategoriesShearchDropDownList.DataTextField = "libelle";
            CategoriesShearchDropDownList.DataValueField = "id";
            CategoriesShearchDropDownList.DataBind();
            //
            CategoriesShearchDropDownList.Items.Insert(0, new ListItem("--", "0"));
            //
            CategoriesShearchDropDownList.SelectedIndex = 0;
            }
        }

        protected void SearchBooksSubmit_Click(object sender, EventArgs e)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            IQueryable<Books> res = DB.Books.AsQueryable();

            if (search_name.Text.Trim() != "")
            {
                res = res.Where(b => b.nom.Contains(search_name.Text.Trim()));
            }
            if (Calendar1.SelectedDate != DateTime.MinValue && Calendar2.SelectedDate != DateTime.MinValue)
                res = res.Where(b => b.creation_date > Calendar1.SelectedDate && b.creation_date < Calendar2.SelectedDate);

            if (CategoriesShearchDropDownList.SelectedValue != "0")
                res = res.Where(b => b.category_id.ToString() == CategoriesShearchDropDownList.SelectedValue);

            if(price_max .Text != "" && price_min.Text != "")
            {
                int min = int.Parse(price_min.Text);
                int max = int.Parse(price_max.Text);
                //
                res = res.Where(b => b.prix <= max && b.prix >= min);
            }
            //
            GridView1.DataSource = res.ToList();
            GridView1.DataBind();
        }

        protected void SearchBooksSubmit2_Click(object sender, EventArgs e)
        {
            EcommerceDBEntities DB = new EcommerceDBEntities();
            //
            string nom = null;
            DateTime? date1 = null, date2 = null;
            int? prix1 = null, prix2 = null, categ = null;
            //
            if (search_name.Text != "")
                nom = search_name.Text;
            if (Calendar1.SelectedDate != DateTime.MinValue) date1 = Calendar1.SelectedDate;
            if (Calendar2.SelectedDate != DateTime.MinValue) date2 = Calendar2.SelectedDate;
            if (CategoriesShearchDropDownList.SelectedValue != "0") categ = int.Parse(CategoriesShearchDropDownList.SelectedValue);
            if (price_min.Text != "") prix1 = int.Parse(price_min.Text);
            if (price_max.Text != "") prix2 = int.Parse(price_max.Text);



            var books = DB.SearchProduit(nom, date1, date2, categ, prix1, prix2);
            GridView1.DataSource = books;
            GridView1.DataBind();
        }
    }
}