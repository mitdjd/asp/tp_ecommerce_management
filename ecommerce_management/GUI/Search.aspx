﻿<%@  Language="C#"  AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="ecommerce_management.GUI.Search" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 522px;
        }
        .auto-style2 {
            width: 816px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Label">search By:</asp:Label>
            <table class="auto-style1">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Category name"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:DropDownList ID="CategoriesShearchDropDownList" runat="server" AutoPostBack="True"></asp:DropDownList>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="prix"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:Label ID="label_price_min" runat="server" Text="Entre"></asp:Label>
                        <asp:TextBox ID="price_min" runat="server" TextMode="Number"></asp:TextBox>
                        <asp:Label ID="label_price_max" runat="server" Text="et"></asp:Label>
                        <asp:TextBox ID="price_max" runat="server" TextMode="Number"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="nom : "></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="search_name" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="nom : "></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:Calendar ID="Calendar1" runat="server" style="display: inline-block"></asp:Calendar>
                        <asp:Calendar ID="Calendar2" runat="server" style="display: inline-block"></asp:Calendar>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="SearchBooksSubmit" runat="server" Text="Search" OnClick="SearchBooksSubmit_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" runat="server">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>