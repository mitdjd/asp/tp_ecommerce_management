﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Site1.Master" AutoEventWireup="true" CodeBehind="BooksGallery.aspx.cs" Inherits="ecommerce_management.GUI.AllCategs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />
    <asp:ListView ID="ListView1" runat="server" >
        <ItemTemplate>
            <a href="BookDetails.aspx?book=<%#Eval("id") %>">
            <div class="book-card">
                <table>
                    <tr>
                        <td>
                            <img src="../Assets/images/<%#Eval("photo") %>" width="200"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="NameLabel" runat="server" CssClass="title" Text='<%#Eval("nom") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="price" Text='<%#Eval("prix") %>' />
                        </td>
                    </tr>
                </table>
            </div>
                </a>


        </ItemTemplate>
    </asp:ListView>

</asp:Content>
