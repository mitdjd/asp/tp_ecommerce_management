﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ecommerce_management.Controller;
using ecommerce_management.Models;
using System.Globalization;

namespace ecommerce_management.GUI
{
    public partial class BookDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("ar-MA");
            //
            int bookId = int.Parse(Request.QueryString["book"]);
            Books book = BooksController.getBook(bookId);
            Session["book"] = book;
            //
            BookTitle.Text = book.nom;
            BookImage.ImageUrl = "~/Assets/images/" + book.photo;
            BookDescription.Text = book.description;
            BookCreationDate.Text = book.creation_date.ToString();
            //
            int category;
            int.TryParse(book.category_id.ToString(), out category);
            CategoryName.Text = "Category : " + CategoriesController.GetCategory(category).libelle;
            //
            BookPrice.Text = book.prix.ToString();
        }

        protected void AddToBasket_Click(object sender, EventArgs e)
        {
            //Books book = (Books)Session["book"];
            ////
            //if(Session["basket"] == null)
            //{
            //    List<Books> bascket = new List<Books>() { book };
            //    Session["basket"] = bascket;
            //} else
            //{
            //    List<Books> bascket = (List<Books>) Session["basket"];
            //    bascket.Add(book);
            //    Session["basket"] = bascket;

            //}
            //((Button)sender).Text = "Book added to basket";
            //
            List<ItemBasket> panier = (List<ItemBasket>)Session["MyBasket"];
            //
            int bookId = int.Parse(Request.QueryString["book"]);
            int qnt = int.Parse(TextBox1.Text);
            //
            var item = panier.FirstOrDefault(i => i.BookID == bookId);
            //
            if (item == null)
                panier.Add(new ItemBasket()
                {
                    BookID = bookId,
                    Quantity = qnt
                });
            else item.Quantity += qnt;

        }
    }
}