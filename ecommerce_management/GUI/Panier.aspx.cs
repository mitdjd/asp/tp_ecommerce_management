﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ecommerce_management.GUI
{
    public partial class Panier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<ItemBasket> panier = (List<ItemBasket>)Session["MyBasket"];
            List<Item> items = new List<Item>();
            Models.EcommerceDBEntities db = new Models.EcommerceDBEntities();
            int total = 0;
            //
            foreach (var item in panier)
            {
                var p = db.Books.FirstOrDefault(n => n.id == item.BookID);
                var newItem = new Item()
                {
                    Category = p.category_id.ToString(),
                    Nom = p.nom,
                    prix = p.prix.Value,
                    Id = p.id,
                    QT = item.Quantity,
                     PT = (item.Quantity * p.prix.Value)
                };
                total += newItem.PT;
                items.Add(newItem);
            }

            GridView1.DataSource = items;
            GridView1.DataBind();
            Label1.Text = total.ToString();
        }
    }
}