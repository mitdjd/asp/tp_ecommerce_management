﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce_management.Models
{
    public partial class contact
    {
        public contact(int age, string nom,string prenom, string email, string tel)
        {
            this.age = age;
            this.nom = nom;
            this.prenom = prenom;
            this.email = email;
            this.tel = tel;
        }
    }
}